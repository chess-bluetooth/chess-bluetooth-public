async (storage, alerteControl, modalControl = null, AppUpdate = null) => {
    const firstTime = localStorage.getItem('explanationAlreadyShown');
    if (firstTime == null) {
        localStorage.setItem('explanationAlreadyShown',true);
        const alertFirstTime = await alerteControl.create({
            header: "♛♔ WELCOME TO BLUETOOTH CHESS ♔♛",
            message: `We're glad to have you! Would you like to see the app user manual first?`,
            buttons: [
                'Ignore',
                {
                    text: '',
                    htmlAttributes: {
                        'aria-label': 'Manual',
                        'innerHTML': '<ion-button><ion-icon name="book"></ion-icon>  <div style="margin-left:5px">Manual</div></ion-button>'
                    },
                    handler: () => {
                        window.open('https://oben-soft-team.blogspot.com/2024/04/bluetooth-chess-manual.html')
                    }
                },
            ],
        });

        await alertFirstTime.present();
    }


}